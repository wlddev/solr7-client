package com.waterlinedata;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.request.SolrPing;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.schema.SchemaResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.*;

public class SolrClientTest {

    private static final Logger logger = LoggerFactory.getLogger(SolrClientTest.class);

    public static void main(String[] args) throws Exception {
        URL resource = SolrClientTest.class.getClassLoader().getResource("solr.properties");
        if (resource == null) {
            throw new RuntimeException("solr.properties not found on the classpath");
        }
        Properties properties = new Properties();
        properties.load(new FileReader(new File(resource.toURI())));

        SolrConnectionDetails solrConnectionDetails = getSolrConnectionDetails(properties);

        SolrPing solrPing;
        SolrPingResponse solrPingResponse;
        int status;

        String test = "all";

        String sampleQuery = properties.getProperty("sampleQuery");

        if (args.length > 0) {
            if ("single".equals(args[0])) {
                test = "single";
            } else if ("cloud".equals(args[0])) {
                test = "cloud";
            }
        }

        SolrConnectionHelper.initializeClient(solrConnectionDetails.getAuthType(), solrConnectionDetails.getUser(), solrConnectionDetails.getPassword());

        if (test.equals("all") || test.equals("single")) {
            solrPing = new SolrPing();
            try (SolrClient singleNodeClient = SolrConnectionHelper.createSingleNodeClient(solrConnectionDetails.getHttpConnectionUrl(), solrConnectionDetails.getCollectionName())) {
                logger.info("********* Standalone Client Test *********");
                doTest(solrPing, sampleQuery, singleNodeClient);
            }
        }


        if (test.equals("all") || test.equals("cloud")) {
            solrPing = new SolrPing();
            try (SolrClient solrCloudClient = SolrConnectionHelper.createSolrCloudClient(solrConnectionDetails.getZkConnectionString(), solrConnectionDetails.getCollectionName())) {
                logger.info("********* Standalone Client Test *********");
                doTest(solrPing, sampleQuery, solrCloudClient);
            }
        }

    }

    private static void doTest(SolrPing solrPing, String sampleQuery, SolrClient singleNodeClient) throws org.apache.solr.client.solrj.SolrServerException, java.io.IOException {
        SolrPingResponse solrPingResponse;
        int status;
        solrPingResponse = solrPing.process(singleNodeClient);
        status = solrPingResponse.getStatus();
        logger.info("Ping status: " + status);

        testFetchExistingFields(singleNodeClient);
        testSaveSampleDocument(singleNodeClient);
        testSampleQuery(singleNodeClient, sampleQuery);
    }

    private static void testSaveSampleDocument(SolrClient solrClient) {
        SolrInputDocument document = new SolrInputDocument();
        String value = "myid-" + UUID.randomUUID();
        document.addField("id", value);
        try {
            solrClient.add(document);
            solrClient.commit();
        } catch (Exception e) {
            logger.error("Error adding document", e);
        }
        logger.info("Added document id: {}", value);
    }

    private static Set<String> testFetchExistingFields(SolrClient solrClient) {
        Set<String> keys = new HashSet<>();
        SchemaRequest.Fields fieldsRequest = new SchemaRequest.Fields();
        SchemaResponse.FieldsResponse fieldsResponse;
        try {
            fieldsResponse = fieldsRequest.process(solrClient);
            List<Map<String, Object>> fields = fieldsResponse.getFields();

            for (Map<String, Object> field : fields) {
                String name = field.get("name").toString();
                logger.debug("Fould existing field {}", name);
                keys.add(name);
            }
        } catch (Exception e) {
            logger.error("Error fetching existing fields", e);
        }
        logger.info("Fetched {} keys", keys.size());
        return keys;

    }

    private static void testSampleQuery(SolrClient solrClient, String queryString) {

        SolrQuery query = new SolrQuery();
        query.setQuery(queryString);

        try {
            long startTime = System.currentTimeMillis();
            QueryResponse queryResponse = solrClient.query(query);
            long numFound = queryResponse.getResults().getNumFound();
            logger.info("numFound: {}", numFound);
            long endTime = System.currentTimeMillis();

            logger.info("Time Taken: " + (endTime - startTime));

            long elapsedTime = queryResponse.getElapsedTime();
            logger.info("elapsedTime: " + elapsedTime);
            int qTime = queryResponse.getQTime();
            logger.info("qTime: " + qTime);

        } catch (Exception e) {
            logger.error("Error running query", e);
        }
    }

    private static SolrConnectionDetails getSolrConnectionDetails(Properties properties) {
        SolrConnectionDetails solrConnectionDetails = new SolrConnectionDetails();
        solrConnectionDetails.setClientMode(SolrConnectionDetails.ClientMode.valueOf(properties.getProperty("clientMode")));
        solrConnectionDetails.setAuthType(SolrConnectionDetails.AuthType.valueOf(properties.getProperty("authType")));
        solrConnectionDetails.setCollectionName(properties.getProperty("collectionName"));
        solrConnectionDetails.setHttpConnectionUrl(properties.getProperty("httpConnectionUrl"));
        solrConnectionDetails.setZkConnectionString(properties.getProperty("zkConnectionString"));
        solrConnectionDetails.setUser(properties.getProperty("user"));
        solrConnectionDetails.setPassword(properties.getProperty("password"));

        return solrConnectionDetails;
    }
}
