package com.waterlinedata;

public class SolrConnectionDetails {

    private ClientMode clientMode;
    private String httpConnectionUrl;
    private String zkConnectionString;
    private String collectionName;
    private AuthType authType = AuthType.NONE;
    private String user;
    private String password;

    public enum ClientMode {
        CLOUD, SINGLE
    }

    public enum AuthType {
        NONE, BASIC, KERBEROS
    }


    public ClientMode getClientMode() {
        return clientMode;
    }

    public void setClientMode(ClientMode clientMode) {
        this.clientMode = clientMode;
    }

    public String getHttpConnectionUrl() {
        return httpConnectionUrl;
    }

    public void setHttpConnectionUrl(String httpConnectionUrl) {
        this.httpConnectionUrl = httpConnectionUrl;
    }

    public String getZkConnectionString() {
        return zkConnectionString;
    }

    public void setZkConnectionString(String zkConnectionString) {
        this.zkConnectionString = zkConnectionString;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public void setAuthType(AuthType authType) {
        this.authType = authType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
