package com.waterlinedata;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ZKUtils {

    public static List<String> parseHosts(String ensemble) {
        ensemble = ensemble.trim();
        int index = ensemble.lastIndexOf("/");
        if(index != -1) {
            ensemble = ensemble.substring(0, index);
        }
        String[] split = ensemble.split(",");
        List<String> strings = Arrays.asList(split);

        List<String> trimmed = strings.stream().map(String::trim).collect(Collectors.toList());
        return trimmed;
    }

    public static String parseChroot(String ensemble) {
        int index = ensemble.lastIndexOf("/");
        if (index != -1) {
            String chroot;
            if (index != ensemble.length()) {
                chroot = ensemble.substring(index);
            } else {
                chroot = null;
            }
            return chroot;
        }
        return null;
    }
}
