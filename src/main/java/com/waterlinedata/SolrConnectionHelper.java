package com.waterlinedata;

import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;
import java.util.Optional;

public class SolrConnectionHelper {

    private static final Logger logger = LoggerFactory.getLogger(SolrConnectionHelper.class);
    private static final String ZK_SASL_CLIENT_ENABLED = "zookeeper.sasl.client";
    private static final String JAAS_CONFIG_FILENAME = "waterlinedata.jaas.config.filename";

    private static HttpRequestInterceptor basicAuthRequestInterceptor;

    public static SolrClient createSingleNodeClient(String baseUrl, String collectionName) {
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        String fullUrl = baseUrl + collectionName;
        HttpSolrClient.Builder builder = new HttpSolrClient.Builder();
        return builder.withBaseSolrUrl(fullUrl).build();
    }

    public static SolrClient createSolrCloudClient(String zkConnectionString, String collectionName) {
        logger.info("Solr cloud connect {}, collection {}", zkConnectionString, collectionName);
        List<String> hosts = ZKUtils.parseHosts(zkConnectionString);
        String chroot = ZKUtils.parseChroot(zkConnectionString);
        CloudSolrClient cloudSolrClient = new CloudSolrClient.Builder(hosts, Optional.ofNullable(chroot)).build();
        cloudSolrClient.setDefaultCollection(collectionName);
        return cloudSolrClient;
    }

    public static void initializeClient(SolrConnectionDetails.AuthType authType, String user, String password) {

        logger.info("Solr authtype {}", authType.name());
        if (authType == SolrConnectionDetails.AuthType.KERBEROS) {
            String jaasConfigFilename = System.getProperty(JAAS_CONFIG_FILENAME);
            if (jaasConfigFilename == null || jaasConfigFilename.isEmpty()) {
                jaasConfigFilename = "waterlinedata-jaas.conf";
                logger.info(JAAS_CONFIG_FILENAME + " not specified in system properties, using default: " + jaasConfigFilename);
            } else {
                logger.info(JAAS_CONFIG_FILENAME + " found in system properties: " + jaasConfigFilename);
            }
            URL resource = SolrConnectionHelper.class.getClassLoader().getResource(jaasConfigFilename);
            if (resource == null) {
                throw new RuntimeException("JAAS config file: " + jaasConfigFilename + " not found in classpath");
            }
            String jaasConfigPath = resource.getPath();
            System.setProperty("java.security.auth.login.config", jaasConfigPath);
            Krb5HttpClientBuilder krb5HttpClientBuilder = new Krb5HttpClientBuilder();
            SolrHttpClientBuilder solrHttpClientBuilder = krb5HttpClientBuilder.getBuilder();
            HttpClientUtil.setHttpClientBuilder(solrHttpClientBuilder);


            // ZK sets the default to true, have to disable it
            System.setProperty(ZK_SASL_CLIENT_ENABLED, Boolean.FALSE.toString());
            logger.info("Kerberos initialized using config file: {} ", jaasConfigPath);
            return;
        }
        // ZK sets the default to true, have to disable it
        System.setProperty(ZK_SASL_CLIENT_ENABLED, Boolean.FALSE.toString());

        if (authType == SolrConnectionDetails.AuthType.BASIC) {
            logger.info("Using basic auth {}/{}", user, password);
            /*final UsernamePasswordCredentials creds = new UsernamePasswordCredentials(user,
                    password);

            if (basicAuthRequestInterceptor != null) {
                HttpClientUtil.removeRequestInterceptor(basicAuthRequestInterceptor);
            }

            basicAuthRequestInterceptor = new HttpRequestInterceptor() {
                @Override
                public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
                    request.addHeader(new BasicScheme().authenticate(creds, request, context));
                }
            };
            HttpClientUtil.addRequestInterceptor(basicAuthRequestInterceptor);*/
            SolrHttpClientBuilder solrHttpClientBuilder = SolrHttpClientBuilder.create();
            solrHttpClientBuilder.setDefaultCredentialsProvider(new SolrHttpClientBuilder.CredentialsProviderProvider() {
                @Override
                public CredentialsProvider getCredentialsProvider() {
                    BasicCredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
                    basicCredentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(user, password));
                    return basicCredentialsProvider;
                }
            });
            /*PreemptiveBasicAuthClientBuilderFactory preemptiveBasicAuthClientBuilderFactory = new PreemptiveBasicAuthClientBuilderFactory();
            SolrHttpClientBuilder httpClientBuilder = preemptiveBasicAuthClientBuilderFactory.getHttpClientBuilder(Optional.of(solrHttpClientBuilder));*/

            HttpClientUtil.setHttpClientBuilder(solrHttpClientBuilder);

        } else {
            if (basicAuthRequestInterceptor != null) {
                HttpClientUtil.removeRequestInterceptor(basicAuthRequestInterceptor);
            }
        }
    }
}
